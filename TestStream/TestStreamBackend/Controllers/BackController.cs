﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Web.Http;
using TestStreamDTO;

namespace TestStreamBackend.Controllers
{
    public class BackController : ApiController
    {

        // POST api/values
        public async Task<IHttpActionResult> Post()
        {
            try
            {
                var binForm = new BinaryFormatter();
                var list = binForm.Deserialize(new BufferedStream(await Request.Content.ReadAsStreamAsync())) as List<XDTO>;
                return Json(new ResponseDto {
                    Message = "backend ok. received " + list.Count + " elements"
                });

            } catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

    }
}
