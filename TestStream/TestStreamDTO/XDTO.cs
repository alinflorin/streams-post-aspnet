﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestStreamDTO
{
    [Serializable]
    public class XDTO
    {
        public string Item1 { get; set; }
        public List<string> Items2 { get; set; }
    }
}
